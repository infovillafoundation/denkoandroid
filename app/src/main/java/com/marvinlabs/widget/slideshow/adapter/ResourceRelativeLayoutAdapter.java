package com.marvinlabs.widget.slideshow.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;

import java.util.Collection;

public class ResourceRelativeLayoutAdapter extends GenericResourceRelativeLayoutAdapter<Integer> {

    public ResourceRelativeLayoutAdapter(Context context, Collection<Integer> resourceIds) {
        super(context, resourceIds);
    }

    public ResourceRelativeLayoutAdapter(Context context, Collection<Integer> resourceIds, BitmapFactory.Options bitmapFactoryOptions) {
        super(context, resourceIds, bitmapFactoryOptions);
    }

    protected int getItemRelativeLayoutResourceId(Integer resourceId, int position) {
        return resourceId;
    }
}
