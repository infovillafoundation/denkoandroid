package com.marvinlabs.widget.slideshow.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import java.util.Collection;

public abstract class GenericResourceRelativeLayoutAdapter<T> extends GenericRelativeLayoutAdapter<T> {

    private BitmapFactory.Options bitmapFactoryOptions;

    public GenericResourceRelativeLayoutAdapter(Context context, Collection<T> items) {
        super(context, items);
    }

    public GenericResourceRelativeLayoutAdapter(Context context, Collection<T> items, BitmapFactory.Options bitmapFactoryOptions) {
        super(context, items);
        this.bitmapFactoryOptions = bitmapFactoryOptions;
    }

    protected abstract int getItemRelativeLayoutResourceId(T item, int position);

    @Override
    protected RelativeLayout asyncLoadRelativeLayout(T item, int position) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return (RelativeLayout) inflater.inflate(getItemRelativeLayoutResourceId(item, position), null);
    }
}
