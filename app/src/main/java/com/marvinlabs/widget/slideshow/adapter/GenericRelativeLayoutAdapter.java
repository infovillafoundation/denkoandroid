package com.marvinlabs.widget.slideshow.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.SparseArray;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class GenericRelativeLayoutAdapter<T> extends RelativeLayoutAdapter {

    private List<T> items;

    private SparseArray<LoadRelativeLayoutTask> runningTasks;

    public GenericRelativeLayoutAdapter(Context context, Collection<T> items) {
        super(context);
        this.items = new ArrayList<T>(items);
        this.runningTasks = new SparseArray<LoadRelativeLayoutTask>(3);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public T getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    protected void loadRelativeLayout(int position) {
        if (position < 0 || position >= items.size()) onRelativeLayoutNotAvailable(position);

        LoadRelativeLayoutTask task = runningTasks.get(position);
        if (task != null) {
            task.cancel(true);
        }

        task = new LoadRelativeLayoutTask(position);
        task.execute(items.get(position));
    }

    @Override
    protected void onRelativeLayoutLoaded(int position, RelativeLayout relativeLayout) {
        Log.d("GenericRLAdapter", "Loading finished for item " + position);
        runningTasks.remove(position);
        super.onRelativeLayoutLoaded(position, relativeLayout);
    }

    @Override
    protected void onRelativeLayoutNotAvailable(int position) {
        Log.d("GenericRLAdapter", "Loading failed for item " + position);
        runningTasks.remove(position);
        super.onRelativeLayoutNotAvailable(position);
    }

    public void shutdown() {
        final int taskCount = runningTasks.size();
        for (int i = 0; i < taskCount; ++i) {
            int key = runningTasks.keyAt(i);
            LoadRelativeLayoutTask t = runningTasks.get(key);
            t.cancel(true);
        }
        runningTasks.clear();
    }

    protected abstract RelativeLayout asyncLoadRelativeLayout(T item, int position);

    private class LoadRelativeLayoutTask extends AsyncTask<T, Void, RelativeLayout> {

        private int position;

        public LoadRelativeLayoutTask(int position) {
            this.position = position;
        }

        protected RelativeLayout doInBackground(T... items) {
            Log.d("GenericRLAdapter", "Loading started for item " + position);

            try {
                RelativeLayout rl = asyncLoadRelativeLayout(items[0], position);

                if (isCancelled()) {
                    return null;
                }

                return rl;
            } catch (Exception e) {
                Log.e("GenericRLAdapter", "Error while loading slide", e);
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(RelativeLayout result) {
            if (result != null) {
                onRelativeLayoutLoaded(position, result);
            } else {
                onRelativeLayoutNotAvailable(position);
            }
        }
    }

}
