package com.marvinlabs.widget.slideshow.adapter;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;

import com.marvinlabs.widget.slideshow.SlideShowAdapter;

import java.lang.ref.WeakReference;

public abstract class RelativeLayoutAdapter extends BaseAdapter implements SlideShowAdapter {

    private static class RelativeLayoutCache {
        SlideStatus status = SlideStatus.LOADING;
        WeakReference<RelativeLayout> relativeLayout = null;
    }

    private Context context;

    private SparseArray<RelativeLayoutCache> cachedRelativeLayouts;

    public RelativeLayoutAdapter(Context context) {
        this.context = context;
        this.cachedRelativeLayouts = new SparseArray<RelativeLayoutCache>(4);
    }

    public Context getContext() {
        return context;
    }

    protected abstract void loadRelativeLayout(int position);

    protected void onRelativeLayoutLoaded(int position, RelativeLayout relativeLayout) {
        RelativeLayoutCache rlc = cachedRelativeLayouts.get(position);
        if (rlc != null) {
            rlc.status = relativeLayout == null ? SlideStatus.NOT_AVAILABLE : SlideStatus.READY;
            rlc.relativeLayout = new WeakReference<RelativeLayout>(relativeLayout);
        }
    }

    protected void onRelativeLayoutNotAvailable(int position) {
        RelativeLayoutCache rlc = cachedRelativeLayouts.get(position);
        if (rlc != null) {
            rlc.status = SlideStatus.NOT_AVAILABLE;
            rlc.relativeLayout = null;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        RelativeLayoutCache rlc = cachedRelativeLayouts.get(position);
        if (rlc == null) {
            prepareSlide(position);
            rlc = cachedRelativeLayouts.get(position);
        }

        if (rlc != null && rlc.status == SlideStatus.READY) {
            RelativeLayout relativeLayout = rlc.relativeLayout.get();
            return relativeLayout;
        }

        return null;
    }

    @Override
    public void prepareSlide(int position) {
        RelativeLayoutCache rlc = cachedRelativeLayouts.get(position);
        if (rlc != null && rlc.relativeLayout != null && rlc.relativeLayout.get() != null) {
            rlc.relativeLayout.clear();
        }

        rlc = new RelativeLayoutCache();
        cachedRelativeLayouts.put(position, rlc);

        loadRelativeLayout(position);
    }

    @Override
    public void discardSlide(int position) {
        RelativeLayoutCache bc = cachedRelativeLayouts.get(position);
        if (bc != null && bc.relativeLayout != null && bc.relativeLayout.get() != null) {
            bc.relativeLayout.clear();
        }
        cachedRelativeLayouts.remove(position);
    }

    @Override
    public SlideStatus getSlideStatus(int position) {
        RelativeLayoutCache rlc = cachedRelativeLayouts.get(position);
        return rlc != null ? rlc.status : SlideStatus.NOT_AVAILABLE;
    }
}
