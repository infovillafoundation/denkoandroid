package com.marvinlabs.widget.slideshow.transition;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.animation.Interpolator;

import com.marvinlabs.widget.slideshow.SlideShowView;

public class SlideUpAndDownTransitionFactory extends BaseTransitionFactory {

    public SlideUpAndDownTransitionFactory() {
        super();
    }

    public SlideUpAndDownTransitionFactory(long duration) {
        super(duration);
    }

    public SlideUpAndDownTransitionFactory(long duration, Interpolator interpolator) {
        super(duration, interpolator);
    }

    @Override
    public Animator getInAnimator(View target, SlideShowView parent, int fromSlide, int toSlide) {
        target.setAlpha(1);
        target.setScaleX(1);
        target.setScaleY(1);
        target.setTranslationX(0);
        target.setTranslationY(1);
        target.setRotationX(0);
        target.setRotationY(0);

        ObjectAnimator animator = ObjectAnimator.ofFloat(target, View.TRANSLATION_Y, 0);
        animator.setDuration(getDuration());
        animator.setInterpolator(getInterpolator());
        return animator;
    }

    @Override
    public Animator getOutAnimator(View target, SlideShowView parent, int fromSlide, int toSlide) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, View.TRANSLATION_Y, 1);
        animator.setDuration(getDuration());
        animator.setInterpolator(getInterpolator());
        return animator;
    }

}
