package org.bitbucket.infovillafoundation.denko.dbhelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.bitbucket.infovillafoundation.denko.dao.DaoMaster;
import org.bitbucket.infovillafoundation.denko.dao.DaoSession;
import org.bitbucket.infovillafoundation.denko.dao.DenkoLastDataState;
import org.bitbucket.infovillafoundation.denko.dao.DenkoLastDataStateDao;
import org.bitbucket.infovillafoundation.denko.dao.DenkoStation;
import org.bitbucket.infovillafoundation.denko.dao.DenkoStationDao;
import org.bitbucket.infovillafoundation.denko.dao.Message;
import org.bitbucket.infovillafoundation.denko.dao.MessageDao;
import org.bitbucket.infovillafoundation.denko.dao.Price;
import org.bitbucket.infovillafoundation.denko.dao.PriceDao;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Sandah Aung on 27/3/15.
 */

public class DenkoDbHelper extends DaoMaster.DevOpenHelper {

    private Context context;

    public DenkoDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        super.onCreate(db);

        String jsonString = readJsonInString("denkostationlist.json", context);

        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession = daoMaster.newSession();

        DenkoStationDao denkoStationDao = daoSession.getDenkoStationDao();
        StationAndLast stationAndLast = convertData(jsonString);
        List<DenkoStation> denkoStations = stationAndLast.getDenkoStations();
        denkoStationDao.insertInTx(denkoStations);

        Calendar calendar = Calendar.getInstance();
        calendar.set(2015, 3, 18, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date date = calendar.getTime();

        MessageDao messageDao = daoSession.getMessageDao();
        Message message = new Message(1L, "Welcome", "BudKqdkyg\\", date);
        messageDao.insert(message);

        PriceDao priceDao = daoSession.getPriceDao();
        Price price = new Price(1L, 0D, 0D, 0D, 0D, date);
        priceDao.insert(price);

        DenkoLastDataStateDao denkoLastDataStateDao = daoSession.getDenkoLastDataStateDao();
        DenkoLastDataState denkoLastDataState = stationAndLast.denkoLastDataState;
        denkoLastDataState.setLastMessageDate(date);
        denkoLastDataState.setLastPriceDate(date);
        denkoLastDataStateDao.insert(denkoLastDataState);
    }

    private StationAndLast convertData(String jsonString) {

        ArrayList<DenkoStation> denkoStations = new ArrayList<>();
        int max = 0;
        int count = 0;
        int sum = 0;

        try {
            JSONArray jsonArray = new JSONArray(jsonString);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                DenkoStation denkoStation = new DenkoStation();

                denkoStation.setId(jsonObject.getLong("id"));
                denkoStation.setStationNameEnglish(jsonObject.getString("stationNameEnglish"));
                denkoStation.setStationNameMyanmar(jsonObject.getString("stationNameMyanmar"));
                denkoStation.setLatitude(jsonObject.getDouble("latitude"));
                denkoStation.setLongitude(jsonObject.getDouble("longitude"));
                denkoStation.setStationAddressEnglish(jsonObject.getString("stationAddressEnglish"));
                denkoStation.setStationAddressMyanmar(jsonObject.getString("stationAddressMyanmar"));

                denkoStations.add(denkoStation);

                max = denkoStation.getId().intValue();
                count++;
                sum += denkoStation.getId().intValue();
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        DenkoLastDataState denkoLastDataState = new DenkoLastDataState(1L, null, null, count, max, sum);

        StationAndLast stationAndLast = new StationAndLast();
        stationAndLast.setDenkoStations(denkoStations);
        stationAndLast.setDenkoLastDataState(denkoLastDataState);

        return stationAndLast;
    }

    private String readJsonInString(String fileName, Context c) {
        try {
            InputStream is = c.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String text = new String(buffer);

            return text;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private class StationAndLast {
        private List<DenkoStation> denkoStations;
        private DenkoLastDataState denkoLastDataState;

        public List<DenkoStation> getDenkoStations() {
            return denkoStations;
        }

        public void setDenkoStations(List<DenkoStation> denkoStations) {
            this.denkoStations = denkoStations;
        }

        public DenkoLastDataState getDenkoLastDataState() {
            return denkoLastDataState;
        }

        public void setDenkoLastDataState(DenkoLastDataState denkoLastDataState) {
            this.denkoLastDataState = denkoLastDataState;
        }
    }
}
