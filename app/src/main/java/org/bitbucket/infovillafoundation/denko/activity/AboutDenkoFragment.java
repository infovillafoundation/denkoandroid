package org.bitbucket.infovillafoundation.denko.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marvinlabs.widget.slideshow.SlideShowAdapter;
import com.marvinlabs.widget.slideshow.SlideShowView;
import com.marvinlabs.widget.slideshow.adapter.ResourceRelativeLayoutAdapter;

import org.bitbucket.infovillafoundation.denko.R;

import java.util.Arrays;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class AboutDenkoFragment extends Fragment {

    ResourceRelativeLayoutAdapter resourceRelativeLayoutAdapter;

    @InjectView(R.id.about_slide_show_area)
    SlideShowView slideShowView;

    @InjectView(R.id.dot_1)
    View dotOne;

    @InjectView(R.id.dot_2)
    View dotTwo;

    @InjectView(R.id.dot_3)
    View dotThree;

    @InjectView(R.id.dot_4)
    View dotFour;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.about_fragment_layout, container, false);
        ButterKnife.inject(this, v);

        return v;
    }

    private SlideShowAdapter createSlideShowAdapter() {
        Integer[] slideShowResources = new Integer[]{R.layout.denko_description, R.layout.denko_product_list, R.layout.number_one_layout, R.layout.contact_layout};
        resourceRelativeLayoutAdapter = new ResourceRelativeLayoutAdapter(getActivity(), Arrays.asList(slideShowResources));
        return resourceRelativeLayoutAdapter;
    }

    @Override
    public void onResume() {
        super.onResume();
        startSlideShow();
    }

    @Override
    public void onStop() {
        resourceRelativeLayoutAdapter.shutdown();
        super.onStop();
    }

    private void startSlideShow() {
        slideShowView.setAdapter(createSlideShowAdapter());
        slideShowView.play();

        slideShowView.setOnSlideShowEventListener(new SlideShowView.OnSlideShowEventListener() {
            @Override
            public void beforeSlideShown(SlideShowView parent, int position) {

            }

            @Override
            public void onSlideShown(SlideShowView parent, int position) {

            }

            @Override
            public void beforeSlideHidden(SlideShowView parent, int position) {
                dotOne.setBackgroundResource(R.drawable.green_circle_hollow);
                dotTwo.setBackgroundResource(R.drawable.green_circle_hollow);
                dotThree.setBackgroundResource(R.drawable.green_circle_hollow);
                dotFour.setBackgroundResource(R.drawable.green_circle_hollow);

                switch (position) {
                    case 0:
                        dotOne.setBackgroundResource(R.drawable.green_circle_filled);
                        break;
                    case 1:
                        dotTwo.setBackgroundResource(R.drawable.green_circle_filled);
                        break;
                    case 2:
                        dotThree.setBackgroundResource(R.drawable.green_circle_filled);
                        break;
                    case 3:
                        dotFour.setBackgroundResource(R.drawable.green_circle_filled);
                        break;
                    default:
                        dotOne.setBackgroundResource(R.drawable.green_circle_filled);
                        break;
                }
            }

            @Override
            public void onSlideHidden(SlideShowView parent, int position) {

            }
        });
    }
}