package org.bitbucket.infovillafoundation.denko.module;

import org.bitbucket.infovillafoundation.denko.application.DenkoApplication;
import org.bitbucket.infovillafoundation.denko.dao.DaoMaster;
import org.bitbucket.infovillafoundation.denko.dao.DaoSession;
import org.bitbucket.infovillafoundation.denko.dao.DenkoLastDataStateDao;
import org.bitbucket.infovillafoundation.denko.dao.DenkoStationDao;
import org.bitbucket.infovillafoundation.denko.dao.MessageDao;
import org.bitbucket.infovillafoundation.denko.dao.PriceDao;
import org.bitbucket.infovillafoundation.denko.rest.DefaultDenkoStationRestService;
import org.bitbucket.infovillafoundation.denko.rest.DenkoStationRestService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Sandah Aung on 26/3/15.
 */

@Module
public class DenkoStationModule {

    private DaoSession daoSession;

    private DaoSession getDaoSession() {
        if (daoSession == null) {
            DaoMaster daoMaster = new DaoMaster(DenkoApplication.getDatabase());
            daoSession = daoMaster.newSession();
        }
        return daoSession;
    }

    @Provides
    @Singleton
    public DenkoStationDao provideDenkoStationDao() {
        return getDaoSession().getDenkoStationDao();
    }

    @Provides
    @Singleton
    public MessageDao provideMessageDao() {
        return getDaoSession().getMessageDao();
    }

    @Provides
    @Singleton
    public PriceDao providePriceDao() {
        return getDaoSession().getPriceDao();
    }

    @Provides
    @Singleton
    public DenkoLastDataStateDao provideDenkoLastDataStateDao() {
        return getDaoSession().getDenkoLastDataStateDao();
    }

    @Provides
    @Singleton
    public DenkoStationRestService provideDenkoStationRestService() {
        return new DefaultDenkoStationRestService();
    }
}
