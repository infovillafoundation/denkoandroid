package org.bitbucket.infovillafoundation.denko.customui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

/**
 * Created by Sandah Aung on 7/4/15.
 */


public class MyanmarEnglishTextView extends TextView {

    public MyanmarEnglishTextView(Context context) {
        super(context);
        String fontName = "fonts/OpenSans.ttf";
        String language = context
                .getSharedPreferences("org.bitbucket.infovillafoundation.denko", Context.MODE_PRIVATE)
                .getString("org.bitbucket.infovillafoundation.denko.language", "en");
        if (language.equals("my")) {
            fontName = "fonts/WINNWAB.ttf";
            float density = context.getResources().getDisplayMetrics().density;
            setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() + 3 * density);
        }
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontName);
        this.setTypeface(face);
    }

    public MyanmarEnglishTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        String fontName = "fonts/OpenSans.ttf";
        String language = context
                .getSharedPreferences("org.bitbucket.infovillafoundation.denko", Context.MODE_PRIVATE)
                .getString("org.bitbucket.infovillafoundation.denko.language", "en");
        if (language.equals("my")) {
            fontName = "fonts/WINNWAB.ttf";
            float density = context.getResources().getDisplayMetrics().density;
            setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() + 3 * density);
        }
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontName);
        this.setTypeface(face);
    }

    public MyanmarEnglishTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        String fontName = "fonts/OpenSans.ttf";
        String language = context
                .getSharedPreferences("org.bitbucket.infovillafoundation.denko", Context.MODE_PRIVATE)
                .getString("org.bitbucket.infovillafoundation.denko.language", "en");
        if (language.equals("my")) {
            fontName = "fonts/WINNWAB.ttf";
            float density = context.getResources().getDisplayMetrics().density;
            setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() + 3 * density);
        }
        Typeface face = Typeface.createFromAsset(context.getAssets(), fontName);
        this.setTypeface(face);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

}
