package org.bitbucket.infovillafoundation.denko.activity;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import org.bitbucket.infovillafoundation.denko.R;
import org.bitbucket.infovillafoundation.denko.comparator.DenkoStationComparator;
import org.bitbucket.infovillafoundation.denko.component.DaggerDenkoStationComponent;
import org.bitbucket.infovillafoundation.denko.component.DenkoStationComponent;
import org.bitbucket.infovillafoundation.denko.dao.DenkoStation;
import org.bitbucket.infovillafoundation.denko.module.DenkoStationModule;
import org.bitbucket.infovillafoundation.denko.service.DenkoStationService;
import org.bitbucket.infovillafoundation.denko.util.Utils;

import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MapViewFragment extends Fragment {

    @InjectView(R.id.denko_map)
    MapView denkoMap;

    @InjectView(R.id.map_search_text)
    EditText mapSearch;

    @InjectView(R.id.refresh_button)
    RelativeLayout refreshButton;

    DenkoStationService denkoStationService;

    LocationManager mlocManager;
    LocationListener mlocListener;

    DenkoStation assignedDenkoStation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.map_fragment_layout, container, false);
        ButterKnife.inject(this, v);

        DenkoStationComponent component = DaggerDenkoStationComponent.builder().denkoStationModule(new DenkoStationModule()).build();
        denkoStationService = component.provideDenkoStationService();

        denkoMap.onCreate(savedInstanceState);

        Bundle arguements = getArguments();
        if (arguements != null) {
            long passedDenkoStationId = arguements.getLong("selectedDenkoStation");
            assignedDenkoStation = denkoStationService.fetchDenkoStationById(passedDenkoStationId);
        }

        registerLocationListener();

        denkoMap.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                final List<DenkoStation> denkoStations = denkoStationService.fetchAllDenkoStations();
                DenkoStationComparator denkoStationComparator = assignedDenkoStation == null ? new DenkoStationComparator() : new DenkoStationComparator(assignedDenkoStation);
                Collections.sort(denkoStations, denkoStationComparator);

                for (DenkoStation denkoStation : denkoStations) {
                    LatLng latLng = new LatLng(denkoStation.getLatitude(), denkoStation.getLongitude());
                    googleMap.addMarker(new MarkerOptions()
                            .title(denkoStation.getStationAddressEnglish())
                            .position(latLng));
                }

                LatLng currentLatLng = Utils.getCurrentLatLng();
                googleMap.setMyLocationEnabled(true);

                MapsInitializer.initialize(getActivity());

                LatLngBounds.Builder builder = LatLngBounds.builder();
                if (currentLatLng != null) {
                    builder.include(currentLatLng);
                }

                for (int i = 0; i < 3; i++) {
                    builder.include(new LatLng(denkoStations.get(i).getLatitude(), denkoStations.get(i).getLongitude()));
                }

                LatLngBounds bounds = builder.build();
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 15);

                googleMap.animateCamera(cu);
            }
        });

        mapSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String currentString = s.toString();
                DenkoStation denkoStation = denkoStationService.findDenkoStationByName(currentString);

                if (currentString.isEmpty()) {
                    assignedDenkoStation = null;
                } else {
                    if (denkoStation != null) {
                        if (assignedDenkoStation == null || assignedDenkoStation.getId() != denkoStation.getId()) {
                            assignedDenkoStation = denkoStation;
                            updateMap();
                        } else {
                            assignedDenkoStation = denkoStation;
                        }
                        mapSearch.setTextColor(getResources().getColor(R.color.correct_white));
                    } else {
                        mapSearch.setTextColor(getResources().getColor(R.color.incorrect_red));
                    }
                }
            }
        });

        refreshButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mapSearch.setText("");
                assignedDenkoStation = null;
                updateMap();
                return true;
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        denkoMap.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        denkoMap.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        denkoMap.onDestroy();
        mlocManager.removeUpdates(mlocListener);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        denkoMap.onLowMemory();
    }

    private void registerLocationListener() {
        mlocManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        mlocListener = new MapLocationListener();
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mlocListener);
    }

    private void updateMap() {

        final List<DenkoStation> denkoStations = denkoStationService.fetchAllDenkoStations();
        DenkoStationComparator denkoStationComparator = assignedDenkoStation == null ? new DenkoStationComparator() : new DenkoStationComparator(assignedDenkoStation);
        Collections.sort(denkoStations, denkoStationComparator);

        LatLngBounds.Builder builder = LatLngBounds.builder();

        for (int i = 0; i < 3; i++) {
            builder.include(new LatLng(denkoStations.get(i).getLatitude(), denkoStations.get(i).getLongitude()));
        }

        denkoMap.getMap().setMyLocationEnabled(true);

        LatLng currentLatLng = Utils.getCurrentLatLng();
        if (currentLatLng != null) {
            builder.include(currentLatLng);
        }

        LatLngBounds bounds = builder.build();
        MapsInitializer.initialize(getActivity());
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);

        denkoMap.getMap().animateCamera(cu);
    }

    private class MapLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            updateMap();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }
}