package org.bitbucket.infovillafoundation.denko.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import org.bitbucket.infovillafoundation.denko.R;
import org.bitbucket.infovillafoundation.denko.dao.DenkoStation;
import org.bitbucket.infovillafoundation.denko.listenerinterface.IconClickListener;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends FragmentActivity implements DenkoListFragment.ListSelectListener, IconClickListener, StationViewFragment.MapLoadable {

    private SlidingMenu menu;

    @InjectView(R.id.orange_nav_drawer_icon)
    ImageView navDrawerIcon;

    @InjectView(R.id.mm_flag)
    ImageView myanmarFlag;

    Fragment fragment;

    private static boolean languageChangeCalled;
    private int viewId;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);

        navDrawerIcon.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                menu.showMenu(true);
                return false;
            }
        });

        myanmarFlag.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!languageChangeCalled) {
                    languageChangeCalled = true;
                    Intent languageChoiceIntent = new Intent(MainActivity.this, LanguageChoiceActivity.class);
                    languageChoiceIntent.putExtra("source", "main");
                    languageChoiceIntent.putExtra("viewId", viewId);

                    int top = getIntent().getIntExtra("top", 0);
                    int stationIndex = getIntent().getIntExtra("stationIndex", 0);
                    languageChoiceIntent.putExtra("stationIndex", stationIndex);
                    languageChoiceIntent.putExtra("top", top);

                    startActivity(languageChoiceIntent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                    languageChangeCalled = false;
                }
                return false;
            }
        });

        viewId = getIntent().getIntExtra("viewId", 0);

        displayView(viewId);

        menu = new SlidingMenu(this);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu_frame_layout);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.menu_frame, new DenkoListFragment())
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.icon_in_drawer, new DenkoDrawerIconFragment())
                .commit();
    }

    private void displayView(int position) {

        int top = getIntent().getIntExtra("top", 0);
        int stationIndex = getIntent().getIntExtra("stationIndex", 0);

        if (fragment instanceof StationViewFragment) {
            StationViewFragment stationViewFragment = (StationViewFragment) fragment;
            stationIndex = stationViewFragment.getStationIndex();
            top = stationViewFragment.getTop();
            getIntent().putExtra("top", top);
            getIntent().putExtra("stationIndex", stationIndex);
        }

        viewId = position;
        switch (position) {
            case 0:
                fragment = new MainViewFragment();
                break;
            case 1:
                fragment = new StationViewFragment();
                StationViewFragment stationViewFragment = (StationViewFragment) fragment;
                stationViewFragment.setTopAndStationIndex(top, stationIndex);
                break;
            case 2:
                fragment = new MapViewFragment();
                break;
            case 3:
                fragment = new AboutDenkoFragment();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame, fragment).commit();
        } else {

        }
    }

    @Override
    public void onBackPressed() {
        if (menu.isMenuShowing()) {
            menu.showContent();
        } else {
            new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                    .setMessage("Are you sure you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            System.exit(0);
                        }
                    }).setNegativeButton("No", null).show();
        }
    }

    @Override
    public void click() {
        menu.showContent();
    }

    @Override
    public void select(int index) {
        displayView(index);
        menu.showContent();
    }

    @Override
    public void loadMap(DenkoStation denkoStation) {
        Fragment fragment = new MapViewFragment();
        Bundle args = new Bundle();
        args.putLong("selectedDenkoStation", denkoStation.getId());
        fragment.setArguments(args);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment).commit();

    }
}
