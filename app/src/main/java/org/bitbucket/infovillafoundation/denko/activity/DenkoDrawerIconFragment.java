package org.bitbucket.infovillafoundation.denko.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import org.bitbucket.infovillafoundation.denko.R;
import org.bitbucket.infovillafoundation.denko.listenerinterface.IconClickListener;

/**
 * Created by Sandah Aung on 23/4/15.
 */

public class DenkoDrawerIconFragment extends Fragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View result = inflater.inflate(R.layout.icon_in_drawer, null);
        result.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((IconClickListener) getActivity()).click();
                return false;
            }
        });
        return result;
    }
}
