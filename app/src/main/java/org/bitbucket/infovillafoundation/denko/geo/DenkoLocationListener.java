package org.bitbucket.infovillafoundation.denko.geo;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

/**
 * Created by Sandah Aung on 13/4/15.
 */

public class DenkoLocationListener implements LocationListener {

    public static double latitude;
    public static double longitude;
    public static boolean isGpsEnabled;

    public DenkoLocationListener() {
        isGpsEnabled = true;
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        isGpsEnabled = true;
    }

    @Override
    public void onProviderDisabled(String provider) {
        isGpsEnabled = false;
    }
}
