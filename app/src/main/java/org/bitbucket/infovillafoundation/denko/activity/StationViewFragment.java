package org.bitbucket.infovillafoundation.denko.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.bitbucket.infovillafoundation.denko.R;
import org.bitbucket.infovillafoundation.denko.adapter.DenkoStationListAdapter;
import org.bitbucket.infovillafoundation.denko.comparator.DenkoStationComparator;
import org.bitbucket.infovillafoundation.denko.component.DaggerDenkoStationComponent;
import org.bitbucket.infovillafoundation.denko.component.DenkoStationComponent;
import org.bitbucket.infovillafoundation.denko.dao.DenkoStation;
import org.bitbucket.infovillafoundation.denko.module.DenkoStationModule;
import org.bitbucket.infovillafoundation.denko.service.DenkoStationService;

import java.util.Collections;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class StationViewFragment extends Fragment {

    @InjectView(R.id.denko_station_list)
    ListView denkoStationList;

    DenkoStationService denkoStationService;

    private Handler mHandler;
    private Runnable mRunnable;

    private int top;
    private int stationIndex;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.station_fragment_layout, container, false);
        ButterKnife.inject(this, v);

        DenkoStationComponent component = DaggerDenkoStationComponent.builder().denkoStationModule(new DenkoStationModule()).build();
        denkoStationService = component.provideDenkoStationService();

        mRunnable = new Runnable() {
            @Override
            public void run() {
                assignAdapter();
                mHandler.postDelayed(this, 15000);
            }
        };

        mHandler = new Handler();
        mHandler.post(mRunnable);

        return v;
    }

    @Override
    public void onDestroy() {
        mHandler.removeCallbacks(mRunnable);
        super.onDestroy();
    }

    private void assignAdapter() {

        int index = denkoStationList.getFirstVisiblePosition();
        View v = denkoStationList.getChildAt(0);
        int top = (v == null) ? 0 : (v.getTop() - denkoStationList.getPaddingTop());

        if (stationIndex != 0)
            index = stationIndex;

        if (this.top != 0)
            top = this.top;

        this.stationIndex = 0;
        this.top = 0;

        List<DenkoStation> denkoStations = denkoStationService.fetchAllDenkoStations();
        Collections.sort(denkoStations, new DenkoStationComparator());
        final DenkoStationListAdapter adapter = new DenkoStationListAdapter(getActivity(), denkoStations);
        denkoStationList.setAdapter(adapter);

        denkoStationList.setSelectionFromTop(index, top);

        denkoStationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DenkoStation denkoStation = adapter.getItem(position);
                ((MapLoadable) getActivity()).loadMap(denkoStation);
            }
        });
    }

    public int getStationIndex() {
        return denkoStationList.getFirstVisiblePosition();
    }

    public int getTop() {
        View v = denkoStationList.getChildAt(0);
        int top = (v == null) ? 0 : (v.getTop() - denkoStationList.getPaddingTop());
        return top;
    }

    public void setTopAndStationIndex(int top, int stationIndex) {
        this.top = top;
        this.stationIndex = stationIndex;
    }

    public interface MapLoadable {
        void loadMap(DenkoStation denkoStation);
    }
}