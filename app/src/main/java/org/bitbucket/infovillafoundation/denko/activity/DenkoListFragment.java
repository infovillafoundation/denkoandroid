package org.bitbucket.infovillafoundation.denko.activity;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.bitbucket.infovillafoundation.denko.R;
import org.bitbucket.infovillafoundation.denko.adapter.DenkoMenuArrayAdapter;

/**
 * Created by Sandah Aung on 23/4/15.
 */

public class DenkoListFragment extends ListFragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DenkoMenuArrayAdapter adapter = new DenkoMenuArrayAdapter(getActivity(), getResources().getStringArray(R.array.menus));
        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        ((ListSelectListener) getActivity()).select(position);
        super.onListItemClick(l, v, position, id);
    }

    public interface ListSelectListener {
        void select(int index);
    }
}
