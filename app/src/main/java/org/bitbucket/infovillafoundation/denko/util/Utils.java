package org.bitbucket.infovillafoundation.denko.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.maps.model.LatLng;

import org.bitbucket.infovillafoundation.denko.application.DenkoApplication;
import org.bitbucket.infovillafoundation.denko.dao.DenkoStation;
import org.bitbucket.infovillafoundation.denko.geo.DenkoLocationListener;

/**
 * Created by Sandah Aung on 15/4/15.
 */
public class Utils {

    public static LatLng getCurrentLatLng() {
        double distance = 0;

        if (!DenkoLocationListener.isGpsEnabled) {
            return null;
        } else {
            if (DenkoLocationListener.latitude == 0 && DenkoLocationListener.longitude == 0) {
                return null;
            } else {
                return new LatLng(DenkoLocationListener.latitude, DenkoLocationListener.longitude);
            }
        }
    }

    public static String getDistance(double latitude, double longitude) {
        double distance = 0;

        if (!DenkoLocationListener.isGpsEnabled) {
            if (readLanguage().equals("en"))
                return "GPS is off";
            else
                return "*sDyDtufpf ydwfxm;onf";
        }

        if (DenkoLocationListener.isGpsEnabled) {

            if (DenkoLocationListener.latitude == 0 && DenkoLocationListener.longitude == 0) {
                if (readLanguage().equals("en"))
                    return "Wait for GPS";
                else
                    return "*sDyDtufpfudk apmifhyg";
            } else {
                distance = distFrom(latitude, longitude, DenkoLocationListener.latitude, DenkoLocationListener.longitude);
            }
        } else {
            if (readLanguage().equals("en"))
                return "GPS is off";
            else
                return "*sDyDtufpf ydwfxm;onf";
        }

        if (readLanguage().equals("en")) {
            return String.format("%.2f", distance) + " miles";
        } else {
            return String.format("%.2f", distance) + " rdkif";
        }
    }

    public static double getDistanceDouble(DenkoStation denkoStation) {
        if (DenkoLocationListener.latitude == 0 && DenkoLocationListener.longitude == 0)
            return 0;
        return distFrom(denkoStation.getLatitude(), denkoStation.getLongitude(), DenkoLocationListener.latitude, DenkoLocationListener.longitude);
    }

    public static double getDistanceDouble(DenkoStation referenceDenkoStation, DenkoStation denkoStation) {
        return distFrom(denkoStation.getLatitude(), denkoStation.getLongitude(), referenceDenkoStation.getLatitude(), referenceDenkoStation.getLongitude());
    }

    private static double distFrom(double lat1, double lon1, double lat2, double lon2) {
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return 3959 * c;
    }


    private static SharedPreferences obtainSharedPreferences() {
        return DenkoApplication.getAppContext().getSharedPreferences("org.bitbucket.infovillafoundation.denko", Context.MODE_PRIVATE);
    }

    private static String readLanguage() {
        return obtainSharedPreferences().getString("org.bitbucket.infovillafoundation.denko.language", "en");
    }

}
