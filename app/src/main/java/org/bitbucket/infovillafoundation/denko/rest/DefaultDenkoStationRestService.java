package org.bitbucket.infovillafoundation.denko.rest;

import org.bitbucket.infovillafoundation.denko.converter.JacksonConverter;
import org.bitbucket.infovillafoundation.denko.dao.DenkoLastDataState;
import org.bitbucket.infovillafoundation.denko.dao.Price;
import org.bitbucket.infovillafoundation.denko.models.DenkoModel;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.Body;

/**
 * Created by Sandah Aung on 29/3/15.
 */

public class DefaultDenkoStationRestService implements DenkoStationRestService {

    private DenkoStationRestService denkoStationRestService;
    private JacksonConverter jacksonConverter;

    @Override
    public void fetchDenkoModel(@Body DenkoLastDataState denkoLastDataState, Callback<DenkoModel> callback) {
        if (jacksonConverter == null)
            jacksonConverter = new JacksonConverter();
        if (denkoStationRestService == null) {

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://denkoapp.jelastic.skali.net/denkoservice/")
                    .setConverter(jacksonConverter)
                    .build();

            denkoStationRestService = restAdapter.create(DenkoStationRestService.class);
        }

        denkoStationRestService.fetchDenkoModel(denkoLastDataState, callback);
    }

    @Override
    public void fetchPrice(Callback<List<Price>> callback) {
        if (jacksonConverter == null)
            jacksonConverter = new JacksonConverter();
        if (denkoStationRestService == null) {

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://128.199.186.47:8080/DenkoStationServer-1/denkoservice/")
                    //.setEndpoint("http://denkoapp.jelastic.skali.net/denkoservice/")
                    //.setEndpoint("http://10.0.2.2:8080/DenkoStationServer-1/denkoservice/")
                    .setConverter(jacksonConverter)
                    .build();

            denkoStationRestService = restAdapter.create(DenkoStationRestService.class);
        }

        denkoStationRestService.fetchPrice(callback);
    }
}
