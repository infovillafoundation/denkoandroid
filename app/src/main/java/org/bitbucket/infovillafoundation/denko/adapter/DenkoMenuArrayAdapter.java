package org.bitbucket.infovillafoundation.denko.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.bitbucket.infovillafoundation.denko.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Sandah Aung on 12/4/15.
 */

public class DenkoMenuArrayAdapter extends ArrayAdapter<String> {

    private final Context context;
    private final String[] values;

    public DenkoMenuArrayAdapter(Context context, String[] values) {
        super(context, R.layout.menu_list_layout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHolder holder;

        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.menu_list_layout, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        int drawableId;

        if (position == 0)
            drawableId = R.drawable.home;
        else if (position == 1)
            drawableId = R.drawable.gas_pump;
        else if (position == 2)
            drawableId = R.drawable.map;
        else
            drawableId = R.drawable.us;

        holder.image.setImageResource(drawableId);
        holder.text.setText(values[position]);

        return view;
    }

    static class ViewHolder {
        @InjectView(R.id.menu_icon)
        ImageView image;
        @InjectView(R.id.menu_text)
        TextView text;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
