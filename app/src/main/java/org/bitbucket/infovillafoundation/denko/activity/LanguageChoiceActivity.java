package org.bitbucket.infovillafoundation.denko.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.bitbucket.infovillafoundation.denko.R;

import java.util.Locale;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class LanguageChoiceActivity extends Activity {

    @InjectView(R.id.language_image)
    ImageView chosenLanguageImage;

    @InjectView(R.id.chosen_language_text)
    TextView chosenLanguageText;

    @InjectView(R.id.english_language_button)
    RelativeLayout englishLanguageButton;

    @InjectView(R.id.myanmar_language_button)
    RelativeLayout myanmarLanguageButton;

    @InjectView(R.id.count_down_image)
    ImageView countDownImage;

    int countDown;
    private Runnable mRunnable;

    private static boolean languageChangeCalled;

    private String source;
    private int viewId;
    private int stationIndex;
    private int top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_choice);

        ButterKnife.inject(this);

        String language = readLanguage();

        source = getIntent().getStringExtra("source");
        viewId = getIntent().getIntExtra("viewId", 0);
        stationIndex = getIntent().getIntExtra("stationIndex", 0);
        top = getIntent().getIntExtra("top", 0);

        changeUiToLangaugeSetting(language);

        englishLanguageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!languageChangeCalled) {
                    changeLanguage("en");
                    languageChangeCalled = true;
                }
            }
        });

        myanmarLanguageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!languageChangeCalled) {
                    changeLanguage("my");
                    languageChangeCalled = true;
                }
            }
        });

        countDown = 3;
        mRunnable = new Runnable() {
            @Override
            public void run() {
                switch (countDown) {
                    case 3:
                        countDownImage.setImageResource(R.drawable.denko_3);
                        break;
                    case 2:
                        countDownImage.setImageResource(R.drawable.denko_2);
                        break;
                    case 1:
                        countDownImage.setImageResource(R.drawable.denko_1);
                        break;
                    case 0:
                        Intent mainIntent = new Intent(LanguageChoiceActivity.this, MainActivity.class);
                        mainIntent.putExtra("viewId", viewId);
                        mainIntent.putExtra("stationIndex", stationIndex);
                        mainIntent.putExtra("top", top);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        startActivity(mainIntent);
                        languageChangeCalled = false;
                        break;
                    default:
                        break;
                }
                if (countDown > 0)
                    new Handler().postDelayed(mRunnable, 300);
                countDown--;
            }
        };

        if (source.equals("language")) {
            new Handler().postDelayed(mRunnable, 1000);
        }
    }

    @Override
    public void onBackPressed() {
        if (!languageChangeCalled) {
            Intent mainIntent = new Intent(LanguageChoiceActivity.this, MainActivity.class);
            mainIntent.putExtra("viewId", viewId);
            mainIntent.putExtra("stationIndex", stationIndex);
            mainIntent.putExtra("top", top);

            finish();

            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            startActivity(mainIntent);
            languageChangeCalled = false;
        }
    }

    private void changeUiToLangaugeSetting(String language) {
        if (language.equals("en")) {
            chosenLanguageText.setText(R.string.english);
            chosenLanguageImage.setImageResource(R.drawable.united_kingdom);
        } else {
            chosenLanguageText.setText(R.string.myanmar);
            chosenLanguageImage.setImageResource(R.drawable.myanmar);
        }
    }

    private void changeLanguage(String language) {
        writeLanguage(language);
        setLocale(language);
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, LanguageChoiceActivity.class);
        refresh.putExtra("source", "language");
        refresh.putExtra("viewId", viewId);
        refresh.putExtra("stationIndex", stationIndex);
        refresh.putExtra("top", top);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        startActivity(refresh);
        languageChangeCalled = false;
    }

    private SharedPreferences obtainSharedPreferences() {
        return getSharedPreferences("org.bitbucket.infovillafoundation.denko", Context.MODE_PRIVATE);
    }

    private String readLanguage() {
        return obtainSharedPreferences().getString("org.bitbucket.infovillafoundation.denko.language", "en");
    }

    private void writeLanguage(String language) {
        obtainSharedPreferences().edit().putString("org.bitbucket.infovillafoundation.denko.language", language).apply();
    }
}
