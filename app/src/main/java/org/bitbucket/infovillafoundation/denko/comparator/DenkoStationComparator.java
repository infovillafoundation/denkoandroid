package org.bitbucket.infovillafoundation.denko.comparator;

import org.bitbucket.infovillafoundation.denko.dao.DenkoStation;
import org.bitbucket.infovillafoundation.denko.util.Utils;

import java.util.Comparator;

/**
 * Created by Sandah Aung on 15/4/15.
 */
public class DenkoStationComparator implements Comparator<DenkoStation> {

    private DenkoStation referenceDenkoStation;

    public DenkoStationComparator() {

    }

    public DenkoStationComparator(DenkoStation referenceDenkoStation) {
        this.referenceDenkoStation = referenceDenkoStation;
    }

    @Override
    public int compare(DenkoStation lhs, DenkoStation rhs) {
        if (referenceDenkoStation == null)
            return (int) Math.round(Utils.getDistanceDouble(lhs) * 100 - Utils.getDistanceDouble(rhs) * 100);
        else
            return (int) Math.round(Utils.getDistanceDouble(referenceDenkoStation, lhs) * 100 - Utils.getDistanceDouble(referenceDenkoStation, rhs) * 100);
    }
}
