package org.bitbucket.infovillafoundation.denko.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.bitbucket.infovillafoundation.denko.R;
import org.bitbucket.infovillafoundation.denko.component.DaggerDenkoStationComponent;
import org.bitbucket.infovillafoundation.denko.component.DenkoStationComponent;
import org.bitbucket.infovillafoundation.denko.dao.Price;
import org.bitbucket.infovillafoundation.denko.module.DenkoStationModule;
import org.bitbucket.infovillafoundation.denko.service.DenkoStationService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainViewFragment extends Fragment {

    TextView pDiesel;
    TextView qDiesel;
    TextView ron95;
    TextView ron92;

    RelativeLayout pDieselRect;
    RelativeLayout qDieselRect;
    RelativeLayout ron95Rect;
    RelativeLayout ron92Rect;

    DenkoStationService denkoStationService;
    Callback<List<Price>> callback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.main_fragment_layout, container, false);

        pDiesel = (TextView) v.findViewById(R.id.p_diesel);
        qDiesel = (TextView) v.findViewById(R.id.q_diesel);
        ron95 = (TextView) v.findViewById(R.id.ron_95);
        ron92 = (TextView) v.findViewById(R.id.ron_92);

        pDieselRect = (RelativeLayout) v.findViewById(R.id.p_diesel_rect);
        qDieselRect = (RelativeLayout) v.findViewById(R.id.q_diesel_rect);
        ron95Rect = (RelativeLayout) v.findViewById(R.id.ron_95_rect);
        ron92Rect = (RelativeLayout) v.findViewById(R.id.ron_92_rect);

        pDieselRect.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                fetchFuelPrices();
                return true;
            }
        });

        qDieselRect.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                fetchFuelPrices();
                return true;
            }
        });

        ron95Rect.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                fetchFuelPrices();
                return true;
            }
        });

        ron92Rect.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                fetchFuelPrices();
                return true;
            }
        });

        DenkoStationComponent component = DaggerDenkoStationComponent.builder().denkoStationModule(new DenkoStationModule()).build();
        denkoStationService = component.provideDenkoStationService();

        updatePrices(denkoStationService.getPriceDao().loadAll().get(0));

        v.findViewById(R.id.price_bar).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                updatePrices(denkoStationService.getPriceDao().loadAll().get(0));
                return true;
            }
        });

        callback = new Callback<List<Price>>() {
            @Override
            public void success(List<Price> prices, Response response) {
                toast(R.string.server_connection_successful);
                Collections.sort(prices, new Comparator<Price>() {
                    @Override
                    public int compare(Price lhs, Price rhs) {
                        return lhs.getPostDate().compareTo(rhs.getPostDate());
                    }
                });

                denkoStationService.updateDatabaseWithPrice(prices.get(prices.size() - 1));

                updatePrices(denkoStationService.getPriceDao().loadAll().get(0));
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("fail", "fail, cause: " + error.toString());
                toast(R.string.server_connection_failed);
            }

            public void toast(int textId) {
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast_layout,
                        (ViewGroup) getActivity().findViewById(R.id.toast_layout_root));

                TextView text = (TextView) layout.findViewById(R.id.toast_text);
                text.setText(getResources().getString(textId));

                Toast toast = new Toast(getActivity().getApplicationContext());
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
        };

        return v;
    }

    private void fetchFuelPrices() {
        toast(R.string.checking_price);
        denkoStationService.fetchPrice(callback);
        updatePrices(denkoStationService.getPriceDao().loadAll().get(0));
    }

    private void updatePrices(Price price) {
        String qDieselPrice = "-";
        String pDieselPrice = "-";
        String ron95Price = "-";
        String ron92Price = "-";

        if (price != null) {
            if (price.getDieselNormal() != 0.0)
                qDieselPrice = price.getDieselNormal().intValue() + "";
            if (price.getDieselSpecial() != 0.0)
                pDieselPrice = price.getDieselSpecial().intValue() + "";
            if (price.getRon95() != 0.0)
                ron95Price = price.getRon95().intValue() + "";
            if (price.getRon92() != 0.0)
                ron92Price = price.getRon92().intValue() + "";
        }

        String pDieselText = String.format("P Diesel: %s Ks", pDieselPrice);
        String qDieselText = String.format("Diesel: %s Ks", qDieselPrice);
        String ron95Text = String.format("Ron 95: %s Ks", ron95Price);
        String ron92Text = String.format("Ron 92: %s Ks", ron92Price);

        pDiesel.setText(pDieselText);
        qDiesel.setText(qDieselText);
        ron95.setText(ron95Text);
        ron92.setText(ron92Text);
    }

    private void toast(int textId) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout,
                (ViewGroup) getActivity().findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.toast_text);
        text.setText(getResources().getString(textId));

        Toast toast = new Toast(getActivity().getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }
}