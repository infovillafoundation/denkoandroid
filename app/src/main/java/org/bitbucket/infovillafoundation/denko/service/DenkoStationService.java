package org.bitbucket.infovillafoundation.denko.service;

import org.bitbucket.infovillafoundation.denko.dao.DenkoLastDataState;
import org.bitbucket.infovillafoundation.denko.dao.DenkoLastDataStateDao;
import org.bitbucket.infovillafoundation.denko.dao.DenkoStation;
import org.bitbucket.infovillafoundation.denko.dao.DenkoStationDao;
import org.bitbucket.infovillafoundation.denko.dao.MessageDao;
import org.bitbucket.infovillafoundation.denko.dao.Price;
import org.bitbucket.infovillafoundation.denko.dao.PriceDao;
import org.bitbucket.infovillafoundation.denko.models.DenkoModel;
import org.bitbucket.infovillafoundation.denko.rest.DenkoStationRestService;

import java.util.List;

import javax.inject.Inject;

import retrofit.Callback;

/**
 * Created by Sandah Aung on 26/3/15.
 */

public class DenkoStationService {

    DenkoStationDao denkoStationDao;
    MessageDao messageDao;
    PriceDao priceDao;
    DenkoLastDataStateDao denkoLastDataStateDao;
    DenkoStationRestService denkoStationRestService;

    @Inject
    public DenkoStationService(DenkoStationDao denkoStationDao, MessageDao messageDao, PriceDao priceDao, DenkoLastDataStateDao denkoLastDataStateDao, DenkoStationRestService denkoStationRestService) {
        this.denkoStationDao = denkoStationDao;
        this.messageDao = messageDao;
        this.priceDao = priceDao;
        this.denkoLastDataStateDao = denkoLastDataStateDao;
        this.denkoStationRestService = denkoStationRestService;
    }

    public List<DenkoStation> fetchAllDenkoStations() {
        return denkoStationDao.loadAll();
    }

    public void fetchDenkoModel(DenkoLastDataState denkoLastDataState, Callback<DenkoModel> callback) {
        denkoStationRestService.fetchDenkoModel(denkoLastDataState, callback);
    }

    public void fetchPrice(Callback<List<Price>> callback) {
        denkoStationRestService.fetchPrice(callback);
    }

    public DenkoLastDataState fetchDenkoLastDataState() {
        return denkoLastDataStateDao.loadAll().get(0);
    }

    public void updateDatabaseWithDenkoModel(DenkoModel denkoModel) {
        denkoStationDao.insertOrReplaceInTx(denkoModel.getDenkoStations());
        messageDao.insertOrReplaceInTx(denkoModel.getMessages());
        priceDao.insertOrReplaceInTx(denkoModel.getPrices());
        denkoLastDataStateDao.insertOrReplace(denkoModel.getDenkoLastDataState());
    }

    public void updateDatabaseWithPrice(Price price) {
        price.setId(1L);
        priceDao.insertOrReplaceInTx(price);
    }

    public PriceDao getPriceDao() {
        return priceDao;
    }

    public DenkoStation fetchDenkoStationById(long id) {
        return denkoStationDao.load(id);
    }

    public DenkoStation findDenkoStationByName(String name) {
        List<DenkoStation> matchedDenkoStations = denkoStationDao.queryBuilder().where(DenkoStationDao.Properties.StationNameEnglish.like(name + "%")).list();
        return matchedDenkoStations.isEmpty() ? null : matchedDenkoStations.get(0);
    }
}
