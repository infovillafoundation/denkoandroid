package org.bitbucket.infovillafoundation.denko.listenerinterface;

/**
 * Created by Sandah Aung on 18/5/15.
 */
public interface IconClickListener {
    void click();
}
