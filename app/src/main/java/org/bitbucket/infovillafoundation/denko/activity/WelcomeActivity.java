package org.bitbucket.infovillafoundation.denko.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.bitbucket.infovillafoundation.denko.R;
import org.bitbucket.infovillafoundation.denko.component.DaggerDenkoStationComponent;
import org.bitbucket.infovillafoundation.denko.component.DenkoStationComponent;
import org.bitbucket.infovillafoundation.denko.dao.Price;
import org.bitbucket.infovillafoundation.denko.module.DenkoStationModule;
import org.bitbucket.infovillafoundation.denko.service.DenkoStationService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class WelcomeActivity extends Activity {

    @InjectView(R.id.rectangular_orange)
    RelativeLayout denkoWelcomeRelativeLayout;

    @InjectView(R.id.mm_flag)
    ImageView myanmarFlag;

    private static boolean mainCalled;
    private static boolean languageChangeCalled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        ButterKnife.inject(this);

        mainCalled = false;

        myanmarFlag.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!languageChangeCalled) {
                    mainCalled = true;
                    languageChangeCalled = true;
                    Intent languageChoiceIntent = new Intent(WelcomeActivity.this, LanguageChoiceActivity.class);
                    languageChoiceIntent.putExtra("source", "welcome");
                    startActivity(languageChoiceIntent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                    languageChangeCalled = false;
                }
                return false;
            }
        });

        denkoWelcomeRelativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        if (!mainCalled) {
                            mainCalled = true;
                            Intent mainIntent = new Intent(WelcomeActivity.this, MainActivity.class);
                            startActivity(mainIntent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            finish();
                        }
                    }
                };

                new Handler().postDelayed(r, 1000);
                return false;
            }
        });

        Handler splashHandler = new Handler();

        Runnable r = new Runnable() {
            @Override
            public void run() {
                if (!mainCalled) {
                    mainCalled = true;
                    Intent mainIntent = new Intent(WelcomeActivity.this, MainActivity.class);
                    startActivity(mainIntent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();
                }
            }
        };

        DenkoStationComponent component = DaggerDenkoStationComponent.builder().denkoStationModule(new DenkoStationModule()).build();
        final DenkoStationService denkoStationService = component.provideDenkoStationService();

        /*
        Callback<DenkoModel> callback = new Callback<DenkoModel>() {
            @Override
            public void success(DenkoModel denkoModel, Response response) {
                toast(R.string.server_connection_successful);
                denkoStationService.updateDatabaseWithDenkoModel(denkoModel);
            }

            @Override
            public void failure(RetrofitError error) {
                toast(R.string.server_connection_failed);
            }

            public void toast(int textId) {
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast_layout,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                TextView text = (TextView) layout.findViewById(R.id.toast_text);
                text.setText(getResources().getString(textId));

                Toast toast = new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
        };

        denkoStationService.fetchDenkoModel(denkoStationService.fetchDenkoLastDataState(), callback);*/

        Callback<List<Price>> callback = new Callback<List<Price>>() {
            @Override
            public void success(List<Price> prices, Response response) {
                toast(R.string.server_connection_successful);
                Collections.sort(prices, new Comparator<Price>() {
                    @Override
                    public int compare(Price lhs, Price rhs) {
                        return lhs.getPostDate().compareTo(rhs.getPostDate());
                    }
                });

                denkoStationService.updateDatabaseWithPrice(prices.get(prices.size() - 1));
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("fail", "fail, cause: " + error.toString());
                toast(R.string.server_connection_failed);
            }

            public void toast(int textId) {
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.toast_layout,
                        (ViewGroup) findViewById(R.id.toast_layout_root));

                TextView text = (TextView) layout.findViewById(R.id.toast_text);
                text.setText(getResources().getString(textId));

                Toast toast = new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();
            }
        };

        denkoStationService.fetchPrice(callback);

        if (isNetworkAvailable())
            splashHandler.postDelayed(r, 10000);
        else {
            toast(R.string.no_internet_connection);
            splashHandler.postDelayed(r, 10000);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    private void toast(int textId) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout,
                (ViewGroup) findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.toast_text);
        text.setText(getResources().getString(textId));

        Toast toast = new Toast(getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }
}
