package org.bitbucket.infovillafoundation.denko.rest;

import org.bitbucket.infovillafoundation.denko.dao.DenkoLastDataState;
import org.bitbucket.infovillafoundation.denko.dao.Price;
import org.bitbucket.infovillafoundation.denko.models.DenkoModel;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.PUT;

/**
 * Created by Sandah Aung on 29/3/15.
 */

public interface DenkoStationRestService {

    @PUT("/denko/")
    void fetchDenkoModel(@Body DenkoLastDataState denkoLastDataState, Callback<DenkoModel> callback);

    @GET("/price/")
    void fetchPrice(Callback<List<Price>> callback);

}
