package org.bitbucket.infovillafoundation.denko.component;

import org.bitbucket.infovillafoundation.denko.module.DenkoStationModule;
import org.bitbucket.infovillafoundation.denko.service.DenkoStationService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Sandah Aung on 28/3/15.
 */

@Singleton
@Component(modules = {DenkoStationModule.class})
public interface DenkoStationComponent {

    DenkoStationService provideDenkoStationService();
}
