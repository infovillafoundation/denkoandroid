package org.bitbucket.infovillafoundation.denko;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class DenkoDaoGenerator {
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "org.bitbucket.infovillafoundation.denko.dao");

        Entity denkoStation = schema.addEntity("DenkoStation");
        denkoStation.addIdProperty();
        denkoStation.addStringProperty("stationNameEnglish");
        denkoStation.addStringProperty("stationNameMyanmar");
        denkoStation.addDoubleProperty("latitude");
        denkoStation.addDoubleProperty("longitude");
        denkoStation.addStringProperty("stationAddressEnglish");
        denkoStation.addStringProperty("stationAddressMyanmar");

        Entity message = schema.addEntity("Message");
        message.addIdProperty();
        message.addStringProperty("englishMessage");
        message.addStringProperty("myanmarMessage");
        message.addDateProperty("messageDate");

        Entity price = schema.addEntity("Price");
        price.addIdProperty();
        price.addDoubleProperty("ron95");
        price.addDoubleProperty("ron92");
        price.addDoubleProperty("dieselNormal");
        price.addDoubleProperty("dieselSpecial");
        price.addDateProperty("postDate");

        Entity denkoLastDataState = schema.addEntity("DenkoLastDataState");
        denkoLastDataState.addIdProperty();
        denkoLastDataState.addDateProperty("lastPriceDate");
        denkoLastDataState.addDateProperty("lastMessageDate");
        denkoLastDataState.addIntProperty("totalNumberOfStations");
        denkoLastDataState.addIntProperty("lastStationId");
        denkoLastDataState.addIntProperty("stationIdSum");

        new DaoGenerator().generateAll(schema, "app/src/main/java");
    }
}
